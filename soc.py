# This script creates a clickable outline of the subheadings of a text. 
# Suitable if you want to give readers an interactive overview at the beginning for long texts with many subheadings.

import re
import os

def list_txt_files_on_desktop():
    desktop_path = os.path.expanduser("~/Desktop")
    txt_files = [f for f in os.listdir(desktop_path) if f.endswith(".txt")]
    return txt_files

def ask_user_to_choose_file(txt_files):
    print("\nList of available txt files on the desktop:")
    for i, txt_file in enumerate(txt_files, start=1):
        print(f"{i}. {txt_file}")

    while True:
        try:
            choice = input("\nChoose the number of the txt file you want to process or press Enter to exit: ")
            if choice == "":
                return None  # User wants to exit
            choice = int(choice)
            if 1 <= choice <= len(txt_files):
                return txt_files[choice - 1]
            else:
                print("\nInvalid choice. Please enter a valid number.")
        except ValueError:
            print("\nInvalid input. Please enter a valid number.")

def add_ids_to_subheadlines(content):
    subheadlines = re.findall(r'<h3>(.*?)<\/h3>', content)
    if not subheadlines:
        return None, content  # Return None to indicate no subheadlines found

    new_content = content
    for i, subheadline in enumerate(subheadlines, start=1):
        new_content = new_content.replace(f'<h3>{subheadline}</h3>', f'<h3 id="{i}">{subheadline}</h3>', 1)

    return subheadlines, new_content

def create_verankert_file(file_name, subheadlines, content_with_ids):
    verankert_file_name = file_name.replace('.txt', '_ov.txt')
    
    with open(verankert_file_name, 'w', encoding='utf-8') as verankert_file:
        if subheadlines:
            verankert_file.write('<h3 id="overview">Überblick</h3>\n<ul>\n')
            for i, subheadline in enumerate(subheadlines, start=1):
                verankert_file.write(f'<li><a href="#{i}">{subheadline}</a></li>\n')
            verankert_file.write('</ul>\n\n')
        
        verankert_file.write(content_with_ids)

    print(f"\nA neat overview has been added to a copy of your file with the suffix '_ov' on your desktop.")

def process_file(file_name):
    try:
        with open(file_name, 'r', encoding='utf-8') as file:
            content = file.read()

        subheadlines, content_with_ids = add_ids_to_subheadlines(content)

        if subheadlines is None:
            print(f'\nI cannot process "{file_name}", please check for a file with a proper h3-subheadline format.')
            return

        create_verankert_file(file_name, subheadlines, content_with_ids)

    except FileNotFoundError:
        print(f'File "{file_name}" not found.')

def main():
    print("\nWelcome to the Subheadline Overview Creator!")

    while True:
        txt_files = list_txt_files_on_desktop()
        if not txt_files:
            print("\nNo txt files found on the desktop.")
            break

        chosen_file = ask_user_to_choose_file(txt_files)
        if chosen_file is None:
            break
        file_path = os.path.join(os.path.expanduser("~/Desktop"), chosen_file)
        process_file(file_path)

        another_file = input("\nPress 'x' to process another file or Enter to exit: ")
        if another_file != 'x':
            break

    print("\nBye.\n")

if __name__ == "__main__":
    main()
